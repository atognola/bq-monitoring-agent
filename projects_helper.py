from google.cloud import bigquery_reservation_v1
from google.cloud.bigquery_reservation_v1.services.reservation_service import pagers
from google.cloud.bigquery_reservation_v1.types import reservation
from typing import Union, AsyncGenerator
import google.cloud.resourcemanager_v3 as resourcemanager_client
from google.cloud.resourcemanager_v3.types import folders, organizations, projects
import asyncio

# set up the Google Cloud Logging python client library
import google.cloud.logging

logging_client = google.cloud.logging.Client()
logging_client.setup_logging()
# use Python’s standard logging library to send logs to GCP
import logging


async def _parse_projects_from_primitive(
    queue: asyncio.Queue[str],
    projects_client: resourcemanager_client.ProjectsAsyncClient,
    primitive: Union[
        folders.Folder,
        organizations.Organization,
        projects.Project,
        reservation.Assignment,
        str,
    ],
    folders_client: resourcemanager_client.FoldersAsyncClient = None,
) -> None:
    """Given an organization Id as string or an assignment as an object, get all the associated projects."""
    if isinstance(primitive, projects.Project) or (
        isinstance(primitive, str) and "projects/" in primitive
    ):
        try:
            await queue.put(primitive)
        except Exception as e:
            logging.error(str(e))
        return
    if isinstance(primitive, reservation.Assignment):
        await _parse_projects_from_primitive(
            queue, projects_client, primitive.assignee, folders_client
        )
        return
    if (
        isinstance(primitive, folders.Folder)
        or isinstance(primitive, organizations.Organization)
        or (
            isinstance(primitive, str)
            and ("folders/" in primitive or "organizations/" in primitive)
        )
    ):
        if isinstance(primitive, str):
            request_projects = resourcemanager_client.ListProjectsRequest(
                parent=primitive, page_token=""
            )
            request_folders = resourcemanager_client.ListFoldersRequest(
                parent=primitive, page_token=""
            )
        else:
            request_projects = resourcemanager_client.ListProjectsRequest(
                parent=primitive.name, page_token=""
            )
            request_folders = resourcemanager_client.ListFoldersRequest(
                parent=primitive.name, page_token=""
            )

        # Make the request the request for projects
        project_result = projects_client.list_projects(request=request_projects)

        # Handle the response
        result_projects = await project_result
        async for page in result_projects.pages:
            for project in page.projects:
                await queue.put(project.name)
            if result_projects.next_page_token:
                request_projects.page_token = result_projects.next_page_token
                result_projects = await projects_client.list_projects(
                    request=request_projects
                )

        all_folders = []
        # Make the request for folders
        page_result = folders_client.list_folders(request=request_folders)

        # Handle the response
        result_folders = await page_result
        async for page in result_folders.pages:
            for folder in page.folders:
                all_folders.append(folder)

            if result_folders.next_page_token:
                request_folders.page_token = result_folders.next_page_token
                result_folders = await folders_client.list_folders(
                    request=request_folders
                )

        futures = []
        for folder in all_folders:
            futures.append(
                _parse_projects_from_primitive(
                    queue, projects_client, folder, folders_client
                )
            )

        result = await asyncio.gather(*futures, return_exceptions=True)
        return


async def parse_projects_from_primitive(
    projects_client: resourcemanager_client.ProjectsAsyncClient,
    primitive: Union[
        folders.Folder,
        organizations.Organization,
        projects.Project,
        reservation.Assignment,
    ],
    folders_client: resourcemanager_client.FoldersAsyncClient = None,
) -> AsyncGenerator[str, None]:
    project_queue = asyncio.Queue()
    dedup_projects = set()
    future = asyncio.ensure_future(
        _parse_projects_from_primitive(
            project_queue, projects_client, primitive, folders_client
        )
    )
    while not future.done() or not project_queue.empty():
        try:
            project_id = project_queue.get_nowait()
            if not project_id in dedup_projects:
                dedup_projects.add(project_id)
                yield project_id
        except asyncio.queues.QueueEmpty:
            if future.done() and project_queue.empty():
                return
            else:
                await asyncio.sleep(0.1)
    try:
        future.result()
    except Exception as e:
        raise e


async def _parse_projects_from_reservation(
    queue: asyncio.Queue,
    reservation_client: bigquery_reservation_v1.ReservationServiceClient,
    projects_client: resourcemanager_client.ProjectsAsyncClient,
    folders_client: resourcemanager_client.FoldersAsyncClient,
    project_id: str,
    location: str,
    reservation: Union[str, None] = None,
) -> None:
    """Returns details about assignments defined within
    a given admin project and location. Optionally, a reservation
    can be passed as paramter to narrow down the results.
    """
    location = location.lower()
    req = bigquery_reservation_v1.ListAssignmentsRequest(
        parent="projects/{project_id}/locations/{location}/reservations/{reservation}".format(
            project_id=project_id,
            location=location,
            reservation=reservation if reservation is not None else "-",
        )
    )
    assignments = reservation_client.list_assignments(request=req)
    futures = []
    for page in assignments.pages:
        for assignment in page.assignments:
            futures.append(
                asyncio.ensure_future(
                    _parse_projects_from_primitive(
                        queue, projects_client, assignment, folders_client
                    )
                )
            )
        if assignments.next_page_token:
            req.page_token = assignments.next_page_token
            assignments = reservation_client.list_assignments(request=req)
    results = await asyncio.gather(*futures, return_exceptions=True)
    for result in results:
        if isinstance(result, Exception):
            raise result
    return


async def parse_projects_from_reservation(
    reservation_client: bigquery_reservation_v1.ReservationServiceClient,
    projects_client: resourcemanager_client.ProjectsAsyncClient,
    folders_client: resourcemanager_client.FoldersAsyncClient,
    project_id: str,
    location: str,
    reservation: Union[str, None] = None,
) -> AsyncGenerator[str, None]:
    project_queue = asyncio.Queue()
    dedup_projects = set()
    future = asyncio.ensure_future(
        _parse_projects_from_reservation(
            project_queue,
            reservation_client,
            projects_client,
            folders_client,
            project_id,
            location,
            reservation,
        )
    )
    while not future.done() or not project_queue.empty():
        try:
            project_id = project_queue.get_nowait()
            if not project_id in dedup_projects:
                dedup_projects.add(project_id)
                yield project_id
        except asyncio.queues.QueueEmpty:
            if future.done() and project_queue.empty():
                return
            else:
                await asyncio.sleep(0.1)
    try:
        future.result()
    except Exception as e:
        raise e
