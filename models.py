from typing import Optional, List, Union
from typing_extensions import Annotated
from pydantic import BaseModel, validator, model_validator, AfterValidator
from enum import Enum
import regex as re
import logging
from fastapi.exceptions import RequestValidationError
import json

# Used to validate project numbers
PROJECT_MATCHING_REGEX = "^[-_#a-z\d]{5,29}[-_a-z\d]{1}$"
PUB_SUB_TOPIC_MATCHING_REGEX = "^projects\/[\d\w-_]+\/topics\/[\d\w-_]+$"


class BQJobStatus(str, Enum):
    """Enumeration of possible BQ job statuses"""

    done = "done"
    pending = "pending"
    running = "running"


class MetricNames(str, Enum):
    """Enumeration of possible BQ job statuses"""

    slots = "slot_millis"
    shuffle = "shuffle_output_bytes"
    shuffle_spill = "shuffle_output_bytes_spilled"
    run_time = "run_time_seconds"


class SupportedNotificationChannels(str, Enum):
    """Enumeration of possible notification channels"""

    pub_sub = "pub_sub"


def validate_project_id_list(list):
    # Validate or raise ValueError
    if len(list) < 1:
        raise RequestValidationError("Empty or invalid list of project IDs provided")
    for project_id in list:
        if project_id < 1:
            logging.error("Invalid project ID being parsed")
            raise RequestValidationError("Invalid project ID in request body")
    return list


ProjectsList = Annotated[
    List[int], AfterValidator(lambda v: validate_project_id_list(v))
]


class AdminConfig(BaseModel):
    admin_project: str
    location: str
    exclude_projects: Optional[ProjectsList] = None

    @model_validator(mode="after")
    def check_admin_config(self) -> "AdminConfig":
        if self.admin_project == "":
            raise RequestValidationError("Please provide a valid admin project Id.")
        if self.location == "":
            raise RequestValidationError(
                "Please provide a valid location, or else leave it empty."
            )
        return self


class OrgConfig(BaseModel):
    """Organization ID based configuration of the poll request"""

    org: int
    exclude_projects: Optional[ProjectsList] = None


class ProjectsConfig(BaseModel):
    list_projects: ProjectsList


PollConfig = Union[AdminConfig, OrgConfig, ProjectsConfig]


class AlertNotificationChannel(BaseModel):
    """Enumeration of possible BQ job statuses"""

    channel: SupportedNotificationChannels
    url: str

    @model_validator(mode="after")
    def check_notification_channel(self) -> "AlertNotificationChannel":
        if self.channel == SupportedNotificationChannels.pub_sub:
            # validate URL to be a valid pub sub one!
            p = re.compile(PUB_SUB_TOPIC_MATCHING_REGEX)
            all_matches = p.findall(self.url)
            if len(all_matches) != 1:
                logging.error("Invalid Pub/Sub url in request body")
                raise RequestValidationError("Invalid Pub/Sub url in request body")
                pass
        return self


class AlertConfig(BaseModel):
    metric_name: MetricNames
    threshold: int
    notification_channel: AlertNotificationChannel

    @model_validator(mode="after")
    def check_alert_config(self) -> "AlertConfig":
        if self.threshold <= 0:
            logging.error(
                "Invalid alert threshold in alert in request body. Please provide a non zero positive integer for threshold"
            )
            raise RequestValidationError(
                "Invalid alert threshold in alert in request body. Please provide a non zero positive integer for threshold"
            )
        return self


class PollRequest(BaseModel):
    """Model defintion to be used as the body of the request for the API.
    The information in the body is integrity checked for valid project ids and other configurations. In the event that any sanity check fails, the API will responde a 422 HTTP error.

    # Parameters:

    1.request (Request): The Uvicorn/FastAPI request object
    2.pollrequest (PollRequest): The input sanitized body of the post request containing the filtering conditions

    Filter status: Can be either done, running or pending.
    Done jobs are only returned if they finished within the timeframe between now and now (time the API was called) - poll_period.

    Returns:
    PollRequest:Sanitized object
    """

    poll_config: PollConfig
    max_jobs: Optional[int] = 1000
    query_job_timeout: Optional[int] = 360
    poll_period: Optional[int] = 1
    filter_status: Optional[BQJobStatus] = None
    alerts_config: Optional[List[AlertConfig]] = None

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "poll_config": {
                        "admin_project": "project1",
                        "location": "eu",
                        "exclude_projects": ["project1", "project2"],
                    },
                    "max_jobs": 1000,
                    "query_job_timeout": 360,
                    "poll_period": 1,
                    "filter_status": "running",
                    "alerts_config": [
                        {
                            "metric_name": "slot_millis",
                            "threshold": 100000,
                            "notification_channel": {
                                "channel": "pub_sub",
                                "url": "projects/bq_admin_project/topics/bq-slot-usage-alerts",
                            },
                        },
                        {
                            "metric_name": "shuffle_output_bytes",
                            "threshold": 1000,
                            "notification_channel": {
                                "channel": "pub_sub",
                                "url": "projects/bq_admin_project/topics/bq-shuffle-bytes-alerts",
                            },
                        },
                        {
                            "metric_name": "shuffle_output_bytes_spilled",
                            "threshold": 100,
                            "notification_channel": {
                                "channel": "pub_sub",
                                "url": "projects/bq_admin_project/topics/bq-shuffle-bytes-spilled-alerts",
                            },
                        },
                        {
                            "metric_name": "run_time_seconds",
                            "threshold": 100,
                            "notification_channel": {
                                "channel": "pub_sub",
                                "url": "projects/bq_admin_project/topics/bq-shuffle-bytes-spilled-alerts",
                            },
                        },
                    ],
                }
            ]
        }
    }

    @validator("max_jobs", always=True)
    def validate_max_jobs(cls, value) -> Optional[int]:
        if value is not None:
            if (
                not (
                    isinstance(value, (int, float, complex))
                    and not isinstance(value, bool)
                )
                or value < 1
            ):
                logging.error("Invalid max_jobs parameter")
                raise RequestValidationError("Invalid max_jobs parameter")
            return value
        else:
            return None

    @validator("query_job_timeout", always=True)
    def validate_query_job_timeout(cls, value) -> Optional[int]:
        if value is not None:
            if (
                not (
                    isinstance(value, (int, float, complex))
                    and not isinstance(value, bool)
                )
                or value < 1
            ):
                logging.error("Invalid query_job_timeout parameter")
                raise RequestValidationError("Invalid query_job_timeout parameter")
            return value
        else:
            return None

    @model_validator(mode="before")
    def pre_check_poll_request_body(self) -> "PollRequest":
        if isinstance(self, (bytes, bytearray)):
            self = self.decode("utf8")
            self = json.loads(self)
        return self

    @model_validator(mode="after")
    def check_poll_request_body(self) -> "PollRequest":
        if self.alerts_config is None:
            self.alerts_config = []
        return self
