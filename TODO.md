## Outstanding work or future fixes

- [ ] Make the / page show something useful, instead of redirecting to /docs
- [ ] Fully configure cloud logging to work with the Uvicorn logger
- [x] Build in Pub/sub notifications
- [ ] Build in other nofitifcation channels?
- [x] Implement done job filtering by start and end time
- [ ] Implement linting and style checks
- [ ] Create automated testing cases
- [ ] Implement max recursion depth or safeguards to prevent infinite loops in listing folders and projects from the org config
- [ ] Make calls to list projects and folders be asynchronous
- [x] Discard "BQ API not enabled" errors
- [x] Implement readyness probe
- [ ] Implement liveness probe (a simple one is now online - but should be improved)
- [ ] If needed, implement rate limiting/backoff in API calls