# Cloud run based BQ monitoring agent

*BQ monitoring agent* is a [Cloud Run](https://cloud.google.com/run/docs) application that exposes a _/poll_ endpoint to poll BQ jobs and also renders a _/docs_ page for a quick reference.

Everytime a post request is received at the _/poll_ endpoint, the application will use the BQ client to get a list of jobs and optionally check them against a configure alerting threshold. Please refer to the _body.json_ document to see an example config or to the [Request body explanation](#request-body-explanation) section of this document.

For details on how to use this app in Cloud Code, read the documentation for Cloud Code for [VS Code](https://cloud.google.com/code/docs/vscode/quickstart-cloud-run?utm_source=ext&utm_medium=partner&utm_campaign=CDR_kri_gcp_cloudcodereadmes_012521&utm_content=-) or [IntelliJ](https://cloud.google.com/code/docs/intellij/quickstart-cloud-run?utm_source=ext&utm_medium=partner&utm_campaign=CDR_kri_gcp_cloudcodereadmes_012521&utm_content=-).

### Table of Contents
* [Quick set up](#quick-set-up)
* [How it works](#how-it-works)
* [Request body explanation](#request-body-explanation)
* [Getting Started with VS Code](#getting-started-with-vs-code)
* [Getting Started with IntelliJ](#getting-started-with-intellij)

---
## Quick set up
Please verify that you have enabled the following APIs or services: Cloud Run, Cloud Scheduler, Cloud Build.

It is highly advisable that you create a Service Account for this application. This service account will need to have the following permissions:
 - Cloud Run invoker
 - Biguery Viewer (in as many projects as you will want to poll)
 - Pub/Sub publisher to the select Pub/Sub topic
 - Org level permission: 'roles/resourcemanager.folderViewer'

1. Modify the _body.json_ file to include the list of projects to poll and set the desired alerting thresholds. This is configured by default to a one minute polling interval. If this needs to be changed, the cron schedule in the Cloud scheduler job (in the shape of _'* * * * *'_) and the _"poll_period"_ in the requests body need to be configured accordingly.
2. Run the following commands:
```
./configure.sh
make build
make deploy
make schedule
```
3. To add more than one Cloud schedule, repeat the steps: 1. Configure json.body and 2. _make schedule_ command.

---
## How it works
The app running in cloud run uses the BQ python client to list jobs with the specified filter.

The filtering conditions are included as part of the body of the request to the _/poll_ endpoint. The supported filters are: _done, pending or running_. This filtering condition is optional and can be ommitted.

> **_NOTE:_**  The done jobs will be filtered out in the app if the time at which they finished is not between _T0_ (time the request was sent) and _T1_ (_T0_ minus the _poll-period_). The backend will round down the _T0_ timestamp to the nearest minute, effectively discarding seconds and milliseconds to prevent clock skew.

Also, as part of the configurations in the body of the request, one or more alerting conditions can be set. Each condition has a configurable threshold and an associated metric, one of: _slot_millis, shuffle_output_bytes, shuffle_output_bytes_spilled, run_time_.

Also, for the alerts, a notification channel must be provided. As of now, the single notification channel that is supported is Pub/Sub and it requires a topic to be included in the body. Example: "projects/project-id/topics/bq-alerting".

For each job exceeding the configured threshold, an alert with the following information will sent in the corresponding channel:
```
{"job":job-info, "alert":{"metric": metric-name, "threshold":exceeded-threshold}}
```
For example:
```
{
   "job":{
      "id":"bquxjob_2b441fa0_18affcbea68",
      "type":"query",
      "status":"done",
      "location":"US",
      "project":"my-sample-project",
      "slot_millis":7226619,
      "shuffle_output_bytes":1571123856,
      "shuffle_output_bytes_spilled":0,
      "user_email":"user@domain.com"
   },
   "alert":{
      "metric":"slot_millis",
      "threshold":1000
   }
}
```

---
### Request body explanation
The following is a more detailed explanation of the body of the request being expected by the poller.

```
{
--One or more alerts to be configured
  "alerts_config": [
    {
      --The metric to monitor
      "metric_name": "slot_millis",
      --The channel to emit the alert
      "notification_channel": {
        "channel": "pub_sub",
        "url": "projects/project-id/topics/bq-monitoring"
      },
      --The threshold related to the metric that will trigger the alert
      "threshold": 100000
    },
    ...
  ],
  --An optional filtering condition for the job status. Can be ommitted or can be: done|pending|running
  "filter_status": "done",
  --A polling configuration that can be org wide or all reservations in an admin project for a given location
  ""poll_config"": {"org":[numeric_org_id]} | {"admin_project":[project_id], "location":[eu|us]},
  --An optional maximum to the amount of jobs that will be polled for each project
  "max_jobs": 1000,
  --The API is stateless, so it needs to know the period at which it's being polled. The value shall be in minutes.
  "poll_period": 1,
  --Jobs that started before now minus the amount of minutes here specified will not be polled.
  "query_job_timeout": 360
}
```

---
### Run the app locally with the Cloud Run Emulator
1. Click on the Cloud Code status bar and select 'Run on Cloud Run Emulator'.  
![image](./img/status-bar.png)

2. Use the Cloud Run Emulator dialog to specify your [builder option](https://cloud.google.com/code/docs/vscode/deploying-a-cloud-run-app#deploying_a_cloud_run_service). Cloud Code supports Docker, Jib, and Buildpacks. See the skaffold documentation on [builders](https://skaffold.dev/docs/pipeline-stages/builders/) for more information about build artifact types.  
![image](./img/build-config.png)

3. Click ‘Run’. Cloud Code begins building your image.

4. View the build progress in the OUTPUT window. Once the build has finished, click on the URL in the OUTPUT window to view your live application.  
![image](./img/cloud-run-url.png)

5. To stop the application, click the stop icon on the Debug Toolbar.

---
## Getting Started with VS Code

### Run the app locally with the Cloud Run Emulator
1. Click on the Cloud Code status bar and select 'Run on Cloud Run Emulator'.  
![image](./img/status-bar.png)

2. Use the Cloud Run Emulator dialog to specify your [builder option](https://cloud.google.com/code/docs/vscode/deploying-a-cloud-run-app#deploying_a_cloud_run_service). Cloud Code supports Docker, Jib, and Buildpacks. See the skaffold documentation on [builders](https://skaffold.dev/docs/pipeline-stages/builders/) for more information about build artifact types.  
![image](./img/build-config.png)

3. Click ‘Run’. Cloud Code begins building your image.

4. View the build progress in the OUTPUT window. Once the build has finished, click on the URL in the OUTPUT window to view your live application.  
![image](./img/cloud-run-url.png)

5. To stop the application, click the stop icon on the Debug Toolbar.

---
## Getting Started with IntelliJ

### Run the app locally with the Cloud Run Emulator

#### Define run configuration

1. Click the Run/Debug configurations dropdown on the top taskbar and select 'Edit Configurations'.  
![image](./img/edit-config.png)

2. Select 'Cloud Run: Run Locally' and specify your [builder option](https://cloud.google.com/code/docs/intellij/developing-a-cloud-run-app#defining_your_run_configuration). Cloud Code supports Docker, Jib, and Buildpacks. See the skaffold documentation on [builders](https://skaffold.dev/docs/pipeline-stages/builders/) for more information about build artifact types.  
![image](./img/local-build-config.png)

#### Run the application
1. Click the Run/Debug configurations dropdown and select 'Cloud Run: Run Locally'. Click the run icon.  
![image](./img/config-run-locally.png)

2. View the build process in the output window. Once the build has finished, you will receive a notification from the Event Log. Click 'View' to access the local URLs for your deployed services.  
![image](./img/local-success.png)
