#!/bin/bash
read -p "Pick a service name [bq-agent]: " service
service=${service:-"bq-agent"}
echo Service name: $service
read -p "Pick a region to deploy to [europe-west1]: " region
region=${region:-"europe-west1"}
echo Please create a service account with the necessary permissions: BQ viewer in the projects where the data is, etc.
project_id=$(gcloud config get-value project 2> /dev/null)
full_default_sa="bq-monitoring-agent@${project_id}.iam.gserviceaccount.com"
read -p "Please type the full SA [$full_default_sa]: " sa_name
sa_name=${sa_name:-$full_default_sa}
unauthenticated=""

while true; do
    read -p "Do you want to allow unauthenticated access to the cloud run app? Y/n " yn
    case $yn in
        [Yy]* ) unauthenticated="--allow-unauthenticated"; break;;
        [Nn]* ) unauthenticated="--no-allow-unauthenticated"; break;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "
app_url = \$(shell gcloud run services describe $service --platform managed --project $project_id --region $region --format 'value(status.url)')
commit_hash = \$(shell git rev-parse HEAD)

.PHONY: build deploy schedule

build:
	\$(shell gcloud builds submit --region=$region --tag gcr.io/$project_id/$service:\$(commit_hash) . )

deploy:
	\$(shell gcloud run deploy $service --project $project_id --region $region --service-account $sa_name $unauthenticated --ingress internal-and-cloud-load-balancing --update-env-vars GOOGLE_CLOUD_PROJECT=$project_id --image gcr.io/$project_id/$service:\$(commit_hash) )

schedule:
	\$(shell gcloud scheduler jobs create http $service-poller --schedule='* * * * *' --location=$region --uri=\$(app_url)/poll --oidc-service-account-email=$sa_name  --oidc-token-audience=\$(app_url) --http-method=POST --message-body-from-file=body.json)
" > makefile