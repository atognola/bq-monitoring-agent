from datetime import datetime
from typing import Any, Union
from google.cloud.bigquery.job import CopyJob, ExtractJob, LoadJob, QueryJob, UnknownJob


def filter_done_jobs_by_end_date(job, end_date) -> bool:
    if job.job_type.lower() == "load":
        pass
    job_end_time = getattr(job, "ended", None)
    if job_end_time is not None:
        return job_end_time >= end_date
    else:
        return True


def parse_job_data(
    job: Union[CopyJob, ExtractJob, LoadJob, QueryJob, UnknownJob]
) -> dict[str, Union[str, int, float, None]]:
    raw_api_data = getattr(job, "_properties", None)
    raw_api_statistics = (
        raw_api_data["statistics"] if "statistics" in raw_api_data.keys() else {}
    )
    shuffle_size_ram = None
    shuffle_size_spill = None
    if hasattr(job, "query_plan"):
        shuffle_size_ram = 0
        shuffle_size_spill = 0
        for stage in job.query_plan:
            shuffle_size_ram += (
                stage.shuffle_output_bytes
                if stage.shuffle_output_bytes is not None
                else 0
            )
            shuffle_size_spill += (
                stage.shuffle_output_bytes_spilled
                if stage.shuffle_output_bytes_spilled is not None
                else 0
            )
    slot_millis = getattr(
        job,
        "slot_millis",
        (
            raw_api_statistics["totalSlotMs"]
            if "totalSlotMs" in raw_api_statistics.keys()
            else None
        ),
    )
    if slot_millis is None:
        slot_millis = 0
    run_time = None
    reservation_id = (
        raw_api_statistics["reservation_id"]
        if "reservation_id" in raw_api_statistics.keys()
        else None
    )
    if getattr(job, "state", "PENDING") != "PENDING":
        if getattr(job, "state", "RUNNING") != "RUNNING":
            run_time = getattr(job, "ended") - getattr(job, "started")
        else:
            started_time = getattr(job, "started")
            run_time = datetime.now(tz=started_time.tzinfo) - started_time
        run_time = run_time.total_seconds()
    return {
        "id": job.job_id,
        "type": job.job_type,
        "status": job.state.lower(),
        "location": job.location,
        "project": job.project,
        "slot_millis": slot_millis,
        "shuffle_output_bytes": shuffle_size_ram,
        "shuffle_output_bytes_spilled": shuffle_size_spill,
        "run_time_seconds": run_time,
        "user_email": job.user_email,
        "reservation_id": reservation_id,
    }
