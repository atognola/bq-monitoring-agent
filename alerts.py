import asyncio
from google.cloud import pubsub_v1
import logging
import json
from models import SupportedNotificationChannels
from fastapi import HTTPException

publisher = pubsub_v1.PublisherClient()


def notify_alerts(data, alerts_config) -> asyncio.Future:
    futures = []
    for config in alerts_config:
        exceeding_jobs = [
            job
            for job in data
            if (job[config.metric_name.value] or 0) > config.threshold
        ]
        if config.notification_channel.channel == SupportedNotificationChannels.pub_sub:
            futures.extend(
                [
                    asyncio.wrap_future(
                        publisher.publish(
                            config.notification_channel.url,
                            bytes(
                                json.dumps(
                                    {
                                        "job": job,
                                        "alert": {
                                            "metric": config.metric_name.value,
                                            "threshold": config.threshold,
                                        },
                                    }
                                ),
                                "utf-8",
                            ),
                        )
                    )
                    for job in exceeding_jobs
                ]
            )

    return asyncio.gather(*futures)


async def async_notify_alerts(data, alerts_config):
    future = notify_alerts(data, alerts_config)
    await future
    return
