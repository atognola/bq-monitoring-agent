#!/bin/bash -eu
#
# Copyright 2023 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
A cloud run app to expose an API to do BQ job polling and alerting.
"""
import os
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

# set up the Google Cloud Logging python client library
import google.cloud.logging

logging_client = google.cloud.logging.Client()
logging_client.setup_logging()
# use Python’s standard logging library to send logs to GCP
import logging
from poll_bq import poll_all
from models import PollRequest, AdminConfig, ProjectsConfig, OrgConfig
from projects_helper import (
    parse_projects_from_reservation,
    parse_projects_from_primitive,
)
from google.cloud import bigquery_reservation_v1
import google.cloud.resourcemanager_v3 as resourcemanager_client
from google.cloud.resourcemanager_v3.types import organizations


# get root logger
logger = logging.getLogger(
    __name__
)  # the __name__ resolve to "main" since we are at the root of the project.
# This will get the root logger since no logger in the configuration has this name.

description = """
BQ monitoring agent API helps you do monitoring of BQ jobs by means of polling the BQ list jobs API. 🚀

## Jobs

You will be able to poll:

* **Only done jobs** (_done jobs that finished earlier than the configured poll_period will not be reported_).
* **All jobs** (_done jobs that finished earlier than the configured poll_period will not be reported_).
* **Only running jobs**
* **Only pending jobs**
"""

app = FastAPI(root_path=os.getenv("ROOT_PATH", ""), description=description)
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/img", StaticFiles(directory="img"), name="img")
templates = Jinja2Templates(directory="templates")


@app.on_event("startup")
async def startup():
    # Constructs the client for interacting with the service.
    # Using grpc as the transport method ensures thread safety
    app.state.reservation_client = bigquery_reservation_v1.ReservationServiceClient(
        transport="grpc"
    )
    app.state.projects_client = resourcemanager_client.ProjectsAsyncClient()
    app.state.folders_client = resourcemanager_client.FoldersAsyncClient()


@app.get("/readiness")
async def readiness():
    return {"status": "ok"}


@app.get("/liveness")
async def liveness():
    # TODO: implement checks to verify BQ APIs or a heartbeat
    return {"status": "ok"}


@app.route("/")
async def hello(request: Request):
    logging.info("hello")
    """Return a friendly HTTP greeting."""
    message = "It's running!"

    """Get Cloud Run environment variables."""
    service = os.environ.get("K_SERVICE", "Unknown service")
    revision = os.environ.get("K_REVISION", "Unknown revision")

    # return templates.TemplateResponse("index.html", {"request": request, "message": message, "Service":service, "Revision": revision})
    return RedirectResponse(url="/docs")


@app.post("/poll")
@app.post("/poll/")
async def poll_bq_jobs(request: Request, pollrequest: PollRequest) -> JSONResponse:
    """Function to be executed upon a POST request to either the /poll or /poll/ routes

    Parameters:
    request (Request): The Uvicorn/FastAPI request object
    pollrequest (PollRequest): The input sanitized body of the post request containing the filtering conditions and alerting configurations.

    Returns:
    JSONResponse:Returns a JSON object containing all the jobs that matched the filtering condition in the body of the request.

    """
    if (
        hasattr(pollrequest.poll_config, "exclude_projects")
        and pollrequest.poll_config.exclude_projects is not None
    ):
        exclude_projects = set(pollrequest.poll_config.exclude_projects)
    else:
        exclude_projects = set()
    if isinstance(pollrequest.poll_config, OrgConfig):
        projects_assignment = parse_projects_from_primitive(
            app.state.projects_client,
            organizations.Organization(
                name=f"organizations/{pollrequest.poll_config.org}"
            ),
            app.state.folders_client,
        )

    if isinstance(pollrequest.poll_config, AdminConfig):
        projects_assignment = parse_projects_from_reservation(
            app.state.reservation_client,
            app.state.projects_client,
            app.state.folders_client,
            pollrequest.poll_config.admin_project,
            location=pollrequest.poll_config.location,
        )

    if isinstance(pollrequest.poll_config, ProjectsConfig):
        projects_assignment = pollrequest.poll_config.list_projects

    all_jobs = await poll_all(
        project_ids=projects_assignment,
        max_jobs=pollrequest.max_jobs,
        query_job_timeout=pollrequest.query_job_timeout,
        poll_period=pollrequest.poll_period,
        filter_status=pollrequest.filter_status,
        alerts_config=pollrequest.alerts_config,
        exclude_projects=exclude_projects,
    )

    return JSONResponse({"all_jobs": all_jobs})


if __name__ == "__main__":
    server_port = os.environ.get("PORT", "8080")
    app.run(debug=False, port=server_port, host="0.0.0.0")
