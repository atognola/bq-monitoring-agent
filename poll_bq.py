import concurrent.futures
import threading
import asyncio
from google.cloud import bigquery
import os, re
from typing import Optional, Union, List, Set, AsyncGenerator
from fastapi import HTTPException, Path, Body, Depends
from fastapi.responses import JSONResponse
import logging
from bq_jobs import parse_job_data, filter_done_jobs_by_end_date
from alerts import async_notify_alerts
import datetime
from models import AlertConfig
from google.api_core.exceptions import BadRequest

# TimeToDeadlineTimeout

# create a local context
local = threading.local()


# function for initializing the worker thread
def worker_initializer(local):
    # get the unique name for this thread
    name = threading.current_thread().name
    logging.debug(f"Initializing BQ worker thread {name}")
    local.name = name
    if os.getenv("GOOGLE_CLOUD_PROJECT", None):
        local.bq_client = bigquery.Client(project=os.getenv("GOOGLE_CLOUD_PROJECT"))
    else:
        local.bq_client = bigquery.Client()
    logging.debug(f"Done initializing BQ worker thread {name}")


bq_executor = concurrent.futures.ThreadPoolExecutor(
    max_workers=int(os.getenv("BQ_MAX_THREADS", "20")),
    initializer=worker_initializer,
    initargs=(local,),
)


def get_project_jobs(
    local,
    project_id: int,
    max_jobs: Optional[int],
    query_job_timeout: int,
    filter_status: str,
    poll_period: int,
    now_floor: datetime,
) -> List:
    logging.debug(
        "Getting: {project} from {thread}".format(project=project_id, thread=local.name)
    )

    begin_time = now_floor - datetime.timedelta(minutes=query_job_timeout)
    client = local.bq_client
    jobs = []
    if filter_status is None and max_jobs is None:
        # Get the jobs iterator that will do paginating calls
        jobs = client.list_jobs(
            all_users=True, project=project_id, min_creation_time=begin_time
        )
    elif filter_status is None and max_jobs is not None:
        jobs = client.list_jobs(
            max_results=max_jobs,
            all_users=True,
            project=project_id,
            min_creation_time=begin_time,
        )
    elif filter_status is not None and max_jobs is None:
        jobs = client.list_jobs(
            all_users=True,
            project=project_id,
            min_creation_time=begin_time,
            state_filter=filter_status,
        )
    else:
        jobs = client.list_jobs(
            max_results=max_jobs,
            all_users=True,
            project=project_id,
            min_creation_time=begin_time,
            state_filter=filter_status,
        )
    # Get all the jobs from the iterator and filter by the completion date
    end_time = now_floor - datetime.timedelta(minutes=poll_period)
    try:
        return [job for job in jobs if filter_done_jobs_by_end_date(job, end_time)]
    except BadRequest as e:
        if "has not enabled bigquery" in e.message.lower():
            return []
        else:
            raise


def get_project_id(project_assignment: str) -> str:
    match = re.match(r"projects/(.*)", project_assignment)
    if match is None:
        return project_assignment
    else:
        return match.group(1)


async def async_get_project_jobs(
    project_ids: Union[List[int], AsyncGenerator[str, None]],
    max_jobs: int,
    query_job_timeout: int,
    filter_status: str,
    poll_period: int,
    now_floor: datetime,
    alerts_config: Optional[List[AlertConfig]],
    exclude_projects: Optional[Set[int]],
) -> List:
    futures = []
    if isinstance(project_ids, list):
        for project in project_ids:
            future = bq_executor.submit(
                get_project_jobs,
                local,
                project,
                max_jobs,
                query_job_timeout,
                filter_status,
                poll_period,
                now_floor,
            )
            # Convert the concurrent.futures.Future into an asyncio.Future that is awaitable
            futures.append(asyncio.wrap_future(future, loop=None))
    else:
        async for project in project_ids:
            id = get_project_id(project)
            numeric_id = None
            if id.isdigit():
                numeric_id = int(id)
            if not numeric_id in exclude_projects:
                future = bq_executor.submit(
                    get_project_jobs,
                    local,
                    id,
                    max_jobs,
                    query_job_timeout,
                    filter_status,
                    poll_period,
                    now_floor,
                )
                # Convert the concurrent.futures.Future into an asyncio.Future that is awaitable
                futures.append(asyncio.wrap_future(future, loop=None))
            else:
                logging.debug(f"Skipping project: {id}")
    result = await asyncio.gather(*futures)
    flat_list = []
    for job_list in result:
        if job_list != []:
            flat_list.extend(job_list)
    parsed_data = [parse_job_data(job) for job in flat_list]
    ## Process job data and
    if parsed_data != []:
        await async_notify_alerts(parsed_data, alerts_config)
    return parsed_data


async def poll_all(
    project_ids: Union[List[str], AsyncGenerator[str, None]],
    max_jobs: Optional[int],
    query_job_timeout: int,
    poll_period: int,
    filter_status: Union[str, None],
    alerts_config: Optional[List[AlertConfig]],
    exclude_projects: Set[int] = None,
) -> JSONResponse:
    logging.debug(f"Getting BQ jobs for project: {project_ids}")
    now_floor = datetime.datetime.now(datetime.timezone.utc).replace(
        second=0, microsecond=0
    )
    try:
        return await async_get_project_jobs(
            project_ids,
            max_jobs,
            query_job_timeout,
            filter_status,
            poll_period,
            now_floor,
            alerts_config,
            exclude_projects,
        )
    except Exception as e:
        logging.error(e, exc_info=True)
        raise HTTPException(
            status_code=getattr(e, "code", 500), detail=getattr(e, "message", str(e))
        )
