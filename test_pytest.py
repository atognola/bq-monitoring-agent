from dotenv import load_dotenv
import os
from unittest import mock
from google.cloud import bigquery

import pytest
from fastapi.testclient import TestClient
from poll_bq import bq_executor, async_notify_alerts, parse_job_data, get_project_jobs

from app import app
from models import (
    AlertConfig,
    AlertNotificationChannel,
    PollRequest,
    ProjectsConfig,
    OrgConfig,
    AdminConfig,
    SupportedNotificationChannels
)

from projects_helper import parse_projects_from_primitive, parse_projects_from_reservation

load_dotenv()  # take environment variables from .env.

@pytest.fixture
def dummy_query():
    # Specify project ID and location
    client = bigquery.Client(project=os.getenv("ADMIN_PROJECT"), location=os.getenv("LOCATION"))

    query = """
        SELECT 1=1
    """

    query_job = client.query(query)


@pytest.fixture
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture
def mock_bq_executor():
    backup = bq_executor.submit
    with mock.patch(
        "poll_bq.bq_executor.submit"
    ) as mock_bq_executor:
        mock_bq_executor.side_effect = backup
        yield mock_bq_executor


@pytest.fixture
def mock_async_notify_alerts():
    backup = async_notify_alerts
    with mock.patch(
        "poll_bq.async_notify_alerts"
    ) as mock_notify_alerts:
        mock_notify_alerts.side_effect = backup
        yield mock_notify_alerts


@pytest.fixture
def mock_parse_job_data():
    backup = parse_job_data
    with mock.patch(
        "poll_bq.parse_job_data"
    ) as mock_parse_job_data:
        mock_parse_job_data.side_effect = backup
        yield mock_parse_job_data


@pytest.fixture
def mock_get_project_jobs():
    backup = get_project_jobs
    with mock.patch(
        "poll_bq.get_project_jobs"
    ) as mock_get_project_jobs:
        mock_get_project_jobs.side_effect = backup
        yield mock_get_project_jobs


@pytest.fixture
def mock_parse_projects_from_primitive():
    backup = parse_projects_from_primitive
    with mock.patch(
        "projects_helper.parse_projects_from_primitive"
    ) as mock_parse_projects_from_primitive:
        mock_parse_projects_from_primitive.side_effect = backup
        yield mock_parse_projects_from_primitive


@pytest.fixture
def mock_parse_projects_from_reservation():
    backup = parse_projects_from_reservation
    with mock.patch(
        "projects_helper.parse_projects_from_reservation"
    ) as mock_parse_projects_from_reservation:
        mock_parse_projects_from_reservation.side_effect = backup
        yield mock_parse_projects_from_reservation


@pytest.mark.asyncio
async def test_poll_bq_jobs_org_config(
    client: TestClient,
    mock_bq_executor,
    mock_async_notify_alerts,
    mock_parse_job_data,
    mock_parse_projects_from_primitive,
    dummy_query
):

    # Prepare request body
    poll_request = PollRequest(
        poll_config=OrgConfig(org=int(os.getenv("ORG_ID"))),
        max_jobs=100,
        query_job_timeout=360,
        poll_period=500,
        filter_status="done",
        alerts_config=[
            AlertConfig(
                metric_name="slot_millis",
                threshold=1000,
                notification_channel=AlertNotificationChannel(
                    channel=SupportedNotificationChannels.pub_sub, url=os.getenv("PUB_SUB_TOPIC")
                ),
            )
        ],
    )

    # Send request
    response = client.post("/poll", json=poll_request.model_dump())

    # Assertions
    assert response.status_code == 200
    assert 'all_jobs' in response.json().keys()
    assert isinstance(response.json()['all_jobs'], list)
    assert len(response.json()['all_jobs']) >= 1
    mock_bq_executor.assert_called()
    mock_parse_job_data.assert_called()
    mock_async_notify_alerts.assert_called()


@pytest.mark.asyncio
async def test_poll_bq_jobs_admin_config(
    client: TestClient,
    mock_bq_executor,
    mock_async_notify_alerts,
    mock_parse_job_data,
    mock_parse_projects_from_reservation,
    dummy_query
):

    # Prepare request body
    poll_request = PollRequest(
        poll_config=AdminConfig(admin_project=os.getenv("ADMIN_PROJECT"), location=os.getenv("LOCATION")),
        max_jobs=100,
        query_job_timeout=36000,
        poll_period=50000,
        filter_status="done",
        alerts_config=[
            AlertConfig(
                metric_name="slot_millis",
                threshold=1000,
                notification_channel=AlertNotificationChannel(
                    channel=SupportedNotificationChannels.pub_sub, url=os.getenv("PUB_SUB_TOPIC")
                ),
            )
        ],
    )

    # Send request
    response = client.post("/poll", json=poll_request.model_dump())

    # Assertions
    assert response.status_code == 200
    assert 'all_jobs' in response.json().keys()
    assert isinstance(response.json()['all_jobs'], list)
    mock_bq_executor.assert_called()
    mock_parse_job_data.assert_called()
    mock_async_notify_alerts.assert_called()


@pytest.mark.asyncio
async def test_poll_bq_jobs_projects_config(
    client: TestClient,
    mock_bq_executor,
    mock_async_notify_alerts,
    mock_parse_job_data,
    dummy_query
):

    # Prepare request body
    poll_request = PollRequest(
        poll_config=ProjectsConfig(list_projects=[117409213781]),
        max_jobs=100,
        query_job_timeout=360,
        poll_period=500,
        filter_status="done",
        alerts_config=[
            AlertConfig(
                metric_name="slot_millis",
                threshold=1000,
                notification_channel=AlertNotificationChannel(
                    channel=SupportedNotificationChannels.pub_sub, url=os.getenv("PUB_SUB_TOPIC")
                ),
            )
        ],
    )

    # Send request
    response = client.post("/poll", json=poll_request.model_dump())

    # Assertions
    assert response.status_code == 200
    assert 'all_jobs' in response.json().keys()
    assert isinstance(response.json()['all_jobs'], list)
    mock_bq_executor.assert_called()
    mock_parse_job_data.assert_called()
    mock_async_notify_alerts.assert_called()


@pytest.mark.asyncio
async def test_poll_bq_jobs_returns_404(
    client: TestClient
):

    # Prepare request body
    poll_request = PollRequest(
        poll_config=ProjectsConfig(list_projects=[12345]),
        max_jobs=100,
        query_job_timeout=300,
        poll_period=5,
        filter_status="done",
        alerts_config=[],
    )

    response = client.post("/poll", json=poll_request.model_dump())
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_liveness(client: TestClient):
    response = client.get("/liveness")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}


@pytest.mark.asyncio
async def test_readiness(client: TestClient):
    response = client.get("/readiness")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}
